/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    audioDeviceManager.initialiseWithDefaultDevices(2, 2);
    audioDeviceManager.addAudioCallback(this);
    audioDeviceManager.addMidiInputCallback(String::empty, this);

}

MainComponent::~MainComponent()
{
    audioDeviceManager.removeMidiInputCallback(String::empty, this);
    audioDeviceManager.removeAudioCallback(this);
}

void MainComponent::resized()
{

}

void MainComponent::audioDeviceIOCallback (const float** inputChannelData,
                            int     numInputChannels,
                            float** outputChannelData,
                            int     numOutputChannels,
                            int     numSamples)
{
    DBG("Callback");
    
    const float *inL = inputChannelData[0];
    const float *inR = inputChannelData[1];
    float *outL = outputChannelData[0];
    float *outR = outputChannelData[1];
    
    while (numSamples--)
    {
        *outL = *inL;
        *outR = *inR;
        
        inL++;
        outL++;
        inR++;
        outR++;
        
    }
    
    
    
}

void MainComponent::audioDeviceAboutToStart (AudioIODevice* device)
{
    DBG("About to Start");
}

void MainComponent::audioDeviceStopped()
{
    DBG("Stopped");
}


void MainComponent::handleIncomingMidiMessage (MidiInput* source,
                                const MidiMessage& message)
{
    
}












